# Auxit

LV2 Plugin, which sidechains can be delayed or phase flipped

The 'depth' or Z-Axis position in a mix can be influenced by a predelay in the reverb setting. The longer the predelay - or to be exact, the greater the difference between direct sound and reflctions - the further forward the instrument/voice seems to be. 
As you can see in the picture below, the direct sound reaches the listener much earlier than the first reflections if the sound source is close to.

<img src="https://codeberg.org/dehnhardt/wiki-files/raw/branch/master/auxit/pics/Reflections.png"/>


With this plug-in you can use a common reverb for multiple channels and delay the sends to the bus. You can also rotate the phase. This can be helpful for other applications (e.g. comb filters).

To achieve that, I created an LV2 plugin that abuses the sidechain functionality to create a kind of delayed and or phase-flipped aux.

## HowTo

1) You can add multiple instances of this plugin in a bus track.
2) Add a sidechain input in the Pin Configuration Dialog
3) In stereo tracks, Mixbus / Ardour add both chanels to one sidechain input. Two set it to stereo, in the source track (the track you selected in the sidechain) make a double click on the send header and add the right channel to the right sidechain input.

<img src="https://codeberg.org/dehnhardt/wiki-files/raw/branch/master/auxit/pics/Auxit.png"/>


4) In the plugin, you can select the delay and phase flip

5) Add an reverb to the Bus, set the predelay there to zero and select the appropriate predelays in the "Auxit"-Plugin.

Unfortunately, it's not very straightforward, because you can't see in the plug-in which track it's for. There are probably still possibilities, I'll have to do some more research.


## Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Licensed under the [MIT-License](https://codeberg.org/dehnhardt/ardour-scripts/src/branch/main/LICENSE.md).

