BUNDLE = mkl-auxit.lv2
INSTALL_DIR = /usr/local/lib/lv2


$(BUNDLE): manifest.ttl mkl-auxit.ttl src/mkl-auxit.so
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp manifest.ttl mkl-auxit.ttl src/mkl-auxit.so $(BUNDLE)

src/mkl-auxit.so: src/mkl-auxit.cpp src/mkl-auxit.h src/mkl-auxit.peg
	g++ -shared -fPIC -DPIC src/mkl-auxit.cpp `pkg-config --cflags --libs lv2-plugin` -o src/mkl-auxit.so

src/mkl-auxit.peg: mkl-auxit.ttl
	lv2peg mkl-auxit.ttl src/mkl-auxit.peg

install: $(BUNDLE)
	if [ -d "/usr/lib/lv2" ]; then echo "Dir INSTALL_DIR=/usr/lib exists"; INSTALL_DIR=/usr/lib/lv2; fi
	if [ -d "/usr/local/lib/lv2" ]; then echo "Dir /usr/local/lib exists"; INSTALL_DIR=/usr/local/lib/lv2; fi
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) src/mkl-siwauxititch.so rc/mkl-auxit.peg
