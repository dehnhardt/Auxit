#include "lv2/core/lv2.h"
#include <stdlib.h>
#include <iostream>

#include "mkl-auxit.h"
#include "mkl-auxit.peg"

#define AUXIT_URI "http://punkt-k.de/plugins/mkl-auxit"

void Auxit::connectPort( const uint32_t port, void* data )
{
  switch( port )
  {
    case mkl_auxit_phase_flip:
      std::cout << "set phase" << std::endl;
      phase_flip = (float*)data;
      break;
    case mkl_auxit_delay:
      std::cout << "set delay" << std::endl;
      p_delay = (float*) data;
      break;
    case mkl_auxit_in_l:
      input_l = (float*)data;
      break;
    case mkl_auxit_in_r:
      input_r = (float*)data;
      break;
    case mkl_auxit_sidechain_l:
      sidechain_l = (float*)data;
      break;
    case mkl_auxit_sidechain_r:
      sidechain_r = (float*)data;
      break;
    case mkl_auxit_out_l:
      output_l = (float*)data;
      break;
    case mkl_auxit_out_r:
      output_r = (float*)data;
      break;
  }
}

void Auxit::run (const uint32_t sample_count) 
{
    if( delay != *p_delay )
        setDelay();

    const int phase = *phase_flip == 1. ? -1 : 1;

    for (uint32_t pos = 0; pos < sample_count; pos++)
    {

        write_pos = write_pos + 1 >= buffer_size ? 0 : write_pos + 1;
        read_pos  = write_pos - delay_samples < 0 ? buffer_size + (write_pos - delay_samples) : write_pos - delay_samples;
        if( write_pos >= buffer_size  or read_pos >= buffer_size )
            continue;
        leftBuffer[write_pos]  = sidechain_l[pos];
        rightBuffer[write_pos] = sidechain_r[pos];
        output_l[pos] = input_l[pos] + phase * leftBuffer[read_pos];
        output_r[pos] = input_r[pos] + phase * rightBuffer[read_pos];
    }
}

void Auxit::setDelay(){
    std::cout << "set new delay: " << *p_delay << std::endl;
    float tmpDelay =  *p_delay;
    if( tmpDelay == delay )
        return;
    std::cout << "set new delay 2 " << std::endl;
    if( tmpDelay > delay ){
        for( u_int32_t i = delay; i < tmpDelay; i++ ) {
            leftBuffer[i] = 0;
            rightBuffer[i] = 0;
        }
    }
    delay = tmpDelay;
    delay_samples = sample_rate * delay / 1000;
    std::cout << "set new delay: " << delay << " - samples: " << delay_samples << std::endl;
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{

    Auxit* auxit = nullptr;
    try 
    {
        auxit = new Auxit ( rate );
    } 
    catch (const std::bad_alloc& ba)
    {
        std::cerr << "Failed to allocate memory. Can't instantiate MKL-Auxit" << std::endl;
        return nullptr;
    }
    return auxit;
}

// LV2 Methods

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
  Auxit* auxit = static_cast< Auxit* >( instance );
  if( auxit != nullptr) 
    auxit->connectPort( port, data );
}


static void
activate(LV2_Handle instance)
{}


static void
run(LV2_Handle instance, uint32_t n_samples)
{
  Auxit* auxit = static_cast< Auxit* >( instance );
  if( auxit != nullptr) 
    auxit->run( n_samples );

}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  Auxit* auxit = static_cast< Auxit* >( instance );
  if( auxit != nullptr) 
    delete auxit;
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {AUXIT_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data };

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}