#pragma once

#include <cstdint>

class Auxit
{
    public:

        Auxit( double rate ) : sample_rate( rate ) {
            std::cout << "sample-rate: " << rate << std::endl;
            buffer_size = static_cast<int>(sample_rate) * 3;
            std::cout << "buffersize: " << buffer_size << std::endl;
            leftBuffer = new float[buffer_size + 1];
            rightBuffer = new float[buffer_size + 1];
            read_pos = 0;
            write_pos = 0;
            delay_samples = 0;
            delay = 0;
        };

        ~Auxit() = default;
        void run (const uint32_t sample_count);
        void connectPort (const uint32_t port, void* data);

    private: 
        float* input_l;
        float* input_r;
        float* sidechain_l;
        float* sidechain_r;
        float* output_l;
        float* output_r;
        float* phase_flip;
        float* p_delay;

        float *leftBuffer;
        float *rightBuffer;
        float delay;
        int32_t buffer_size;

        double sample_rate;
        int32_t delay_samples;

        int32_t read_pos;
        int32_t write_pos;

    private: // methods
    void setDelay( );
};
